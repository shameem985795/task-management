# task-management

## Go to master branch 

## Download backend folder in laravel and setup this project by this instructions 

01. Copy .env.example file and rename it by .env file.
02. Open .env file and configure these credentials 
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=task_management
DB_USERNAME=root
DB_PASSWORD=
```
03. Download task_management.sql db and import your mysql databases

04. run this command  
`composer update`

05. Also run this command 
```
php artisan config:cache
php artisan optimize
php artisan cache:clear
```

06. run this project by this command 
`php artisan serve`

## Download frontend folder in ReactJS and setup this project by this instructions

01. Go to this project, open terminal and run this command 
`npm install` 
02. also run this command 
`npm run dev`
03. You get a url by running this command. Copy this url and paste it browser. I hope,  It will run successfully. 
